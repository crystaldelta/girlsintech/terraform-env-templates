### Overview

Modules that create the HSTS secure headers prevention function will be attached to CloudFront.

Lambda function code needs to be pushed as a .zip file so before pushing the changes you need to create the zip file of your lambda function.
To create the zip file run the following command in the terminal
```
zip /tmp/secure_headers.zip secure_headers.js
```

### Directories

| Filename          | Description                                                                                           |
|-------------------|-------------------------------------------------------------------------------------------------------|
| `main.tf`         | Terraform modules creates secure headers.                                                             |
| `outputs.tf`      | Holds the output variables which can be used by other modules throughout the infrastructure.          |
| `variables.tf`    | Holds the variables which will be passed to main.tf.                                                  |
|---------------------------------------------------------------------------------------------------------------------------|
