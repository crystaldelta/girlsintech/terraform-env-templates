provider "aws" {
  region                  = "us-east-1"
  alias                   = "us-east"
}

resource "aws_lambda_function" "secure_headers" {
  provider      = aws.us-east
  filename      = "/tmp/secure_headers.zip"
  function_name = "${var.app_name}-${var.env_name}-secure-headers"
  role          = var.lambda_iam_role
  handler       = "secure_headers.handler"
  publish       = true

  # The filebase64sha256() function is available in Terraform 0.11.12 and later
  # For Terraform 0.11.11 and earlier, use the base64sha256() function and the file() function:
  # source_code_hash = "${base64sha256(file("lambda_function_payload.zip"))}"
  source_code_hash = filebase64sha256("/tmp/secure_headers.zip")

  runtime = "nodejs12.x"

  # environment {
  #   variables = {
  #     Name = "${var.app_name}-${var.env_name}-secure-headers"
  #   }
  # }

  tags = {
    "Name"        = "${var.app_name}-${var.env_name}-secure-headers"
    "Environment" = "${var.app_name}-${var.env_name}"
    "Application" = var.app_name
  }
}