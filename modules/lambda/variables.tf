variable "env_name" {
  description = "Name of the application's environment"
}
variable "app_name" {
  description = "Name of the project"
}
variable "lambda_iam_role" {
  description = "Lambda IAM role."
}