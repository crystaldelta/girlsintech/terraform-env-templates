### Overview

This repository holds all the Terraform modules. 

### Directories

| Directory Name    | Description                                                                                           |
|-------------------|-------------------------------------------------------------------------------------------------------|
| `acm`             | Creates ACM Certificates for the application and validates with Crystaldelta's host.              |
| `admin`           | Creates Jenkins and Bastion server services.                                                          |
| `alb`             | Creates application load balancer for the PMS backend service.                                        |
| `cloudwatch`      | Creates Cloudwatch log groups for ECS.                                                                |
| `ecr`             | Creates ECR repo for the PMS backend.                                                                 |
| `ecs`             | Creates ECS Fargate and task definition for PMS Backend.                                              |
| `iam`             | Creates custom IAM roles and policies which will be used throughout the infra setup.                  |
| `lambda`          | Creates Lambda for the HSTS secure headers.                                                           |
| `rds`             | Creates Database for the PMS application.                                                             |
| `s3`              | Creates S3 buckets for static website hosting, data storage and store logs.                        |
| `secret_manager`  | Creates secret manager to store the application secret values environment variables.                  |
| `security`        | Creates security groups for RDS, ECS, ALB, etc.                                                       |
| `vpc`             | Creates networking components for the application.                                                    |
|---------------------------------------------------------------------------------------------------------------------------|