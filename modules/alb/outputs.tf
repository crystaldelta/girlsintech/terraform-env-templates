output "alb_id" {
  value = aws_alb.main_alb.id
}
output "backend_tg_id" {
  value = aws_lb_target_group.backend_tg
}
output "listener_id" {
  value = aws_lb_listener.https_listener
}