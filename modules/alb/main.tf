resource "aws_alb" "main_alb" {
  name                       = "${var.app_name}-${var.env_name}-alb"
  subnets                    = [var.public_subnet_1, var.public_subnet_2]
  security_groups            = [var.alb_sg]
  idle_timeout               = 60
  enable_deletion_protection = true

  tags = {
    "Name"        = "${var.app_name}-${var.env_name}-alb"
    "Environment" = "${var.app_name}-${var.env_name}"
    "Application" = var.app_name
  }
}

resource "aws_lb_target_group" "backend_tg" {
  name        = "${var.app_name}-${var.env_name}-backend-tg"
  port        = 80
  protocol    = "HTTP"
  vpc_id      = var.vpc_id
  target_type = "ip"

  health_check {
    healthy_threshold   = 3
    unhealthy_threshold = 10
    timeout             = 5
    interval            = 10
    path                = "/api/v1/healthcheck"
  }

  tags = {
    "Name"        = "${var.app_name}-${var.env_name}-backend-tg"
    "Environment" = "${var.app_name}-${var.env_name}"
    "Application" = var.app_name
  }
}

resource "aws_lb_listener" "https_listener" {
  load_balancer_arn = aws_alb.main_alb.id
  port              = 443
  protocol          = "HTTPS"
  ssl_policy        = "ELBSecurityPolicy-2016-08"
  certificate_arn   = var.alb_acm_certificate

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.backend_tg.arn
  }
}

resource "aws_lb_listener_rule" "backend_redirection" {
  listener_arn = aws_lb_listener.https_listener.arn
  priority     = 1

  action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.backend_tg.arn
  }

  condition {
    host_header {
      values = [var.alb_domain_name]
    }
  }

  condition {
    path_pattern {
      values = ["/api/*"]
    }
  }
}
