resource "aws_secretsmanager_secret" "credentials" {
  name = "${var.app_name}-${var.env_name}-secrets-manager"
  recovery_window_in_days = 0
}