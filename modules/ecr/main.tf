resource "aws_ecr_repository" "backend_ecr" {
  name = "${var.app_name}-${var.env_name}-backend-ecr"

  tags = {
    "Name"        = "${var.app_name}-${var.env_name}-backend-ecr"
    "Environment" = "${var.app_name}-${var.env_name}"
    "Application" = var.app_name
  }
}

resource "aws_ecr_repository_policy" "backend_ecrpolicy" {
  repository = aws_ecr_repository.backend_ecr.name

  policy = <<EOF
{
    "Version": "2008-10-17",
    "Statement": [
        {
            "Sid": "new policy",
            "Effect": "Allow",
            "Principal": "*",
            "Action": [
                "ecr:BatchCheckLayerAvailability",
                "ecr:BatchDeleteImage",
                "ecr:BatchGetImage",
                "ecr:CompleteLayerUpload",
                "ecr:DeleteLifecyclePolicy",
                "ecr:DeleteRepository",
                "ecr:DeleteRepositoryPolicy",
                "ecr:DescribeImages",
                "ecr:DescribeRepositories",
                "ecr:GetAuthorizationToken",
                "ecr:GetDownloadUrlForLayer",
                "ecr:GetLifecyclePolicy",
                "ecr:GetLifecyclePolicyPreview",
                "ecr:GetRepositoryPolicy",
                "ecr:InitiateLayerUpload",
                "ecr:ListImages",
                "ecr:PutImage",
                "ecr:PutLifecyclePolicy",
                "ecr:SetRepositoryPolicy",
                "ecr:StartLifecyclePolicyPreview",
                "ecr:UploadLayerPart"
            ]
        }
    ]
}
EOF
}