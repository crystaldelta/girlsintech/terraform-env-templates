variable "env_name" {
  description = "Name of the application's environment"
}
variable "app_name" {
  description = "Name of the project"
}
variable "secret_manager_arn" {
  description = "Secret Manager ARN"
}