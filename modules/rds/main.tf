resource "aws_db_subnet_group" "db_subnet" {
  name       = "${var.app_name}-${var.env_name}-db-subnet-group"
  subnet_ids = [var.db_subnet_1, var.db_subnet_2]

  tags = {
    "Name"        = "${var.app_name}-${var.env_name}-db-subnet-group"
    "Environment" = "${var.app_name}-${var.env_name}"
    "Application" = var.app_name
  }
}

resource "aws_db_instance" "db_instance" {
  allocated_storage           = var.allocated_storage
  engine                      = "mysql"
  engine_version              = "5.7"
  instance_class              = "db.t3.micro"
  name                        = replace("${var.app_name}_${var.env_name}_db", "-", "_")
  identifier                  = "${var.app_name}-${var.env_name}-db"
  username                    = var.mysql_master_user
  password                    = var.mysql_master_password
  parameter_group_name        = "default.mysql5.7"
  vpc_security_group_ids      = [var.db_sg]
  db_subnet_group_name        = aws_db_subnet_group.db_subnet.id
  backup_retention_period     = 35
  copy_tags_to_snapshot       = true
  auto_minor_version_upgrade  = true
  allow_major_version_upgrade = true
  multi_az                    = true
  maintenance_window          = "Sat:21:00-Sat:23:00"
  backup_window               = "02:00-04:00"
  storage_encrypted           = true
  deletion_protection         = true
  ca_cert_identifier          = "rds-ca-2019"

  enabled_cloudwatch_logs_exports = [
    "audit",
    "error",
    "general",
    "slowquery"
  ]

  tags = {
    "Name"        = "${var.app_name}-${var.env_name}-db"
    "Environment" = "${var.app_name}-${var.env_name}"
    "Application" = var.app_name
  }
}