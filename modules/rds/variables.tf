variable "env_name" {
  description = "Name of the application's environment"
}
variable "app_name" {
  description = "Name of the project"
}
variable "db_app_name" {
  description = "Name of the project"
}
variable "db_subnet_1" {
  description = "MySQL DB subnet"
}
variable "db_subnet_2" {
  description = "MySQL DB subnet"
}
variable "db_sg" {
  description = "MySQL DB security group"
}
variable "mysql_master_user" {
  description = "MySQL DB root username"
}
variable "mysql_master_password" {
  description = "MySQL DB root password"
}
variable "allocated_storage" {
  description = "Size of the RDS DB"
}