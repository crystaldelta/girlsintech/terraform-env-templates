provider "aws" {
  region                  = "us-east-1"
  alias                   = "nonprod"
}

provider "aws" {
  region                  = "us-east-1"
  alias                   = "east"
}

data "aws_route53_zone" "domain" {
  name     = var.main_domain_name
  # provider = aws.nonprod

}

resource "aws_acm_certificate" "wildcard_cert" {
  domain_name       = var.main_domain_name
  provider          = aws.east
  validation_method = "DNS"

  subject_alternative_names = [var.wildcard_domain_name]

  tags = {
    "Name"        = "${var.app_name}-${var.env_name}-wildcard-acm"
    "Environment" = "${var.app_name}-${var.env_name}"
    "Application" = var.app_name
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_route53_record" "wildcard_domain_record" {
  # provider = aws.nonprod
  for_each = {
    for dvo in aws_acm_certificate.wildcard_cert.domain_validation_options : dvo.domain_name => {
      name   = dvo.resource_record_name
      record = dvo.resource_record_value
      type   = dvo.resource_record_type
    }
  }

  allow_overwrite = true
  name            = each.value.name
  records         = [each.value.record]
  ttl             = 60
  type            = each.value.type
  zone_id         = data.aws_route53_zone.domain.zone_id
}

resource "aws_acm_certificate_validation" "wildcard_certificate_validation" {
  provider                = aws.east
  certificate_arn         = aws_acm_certificate.wildcard_cert.arn
  validation_record_fqdns = [for record in aws_route53_record.wildcard_domain_record : record.fqdn]
}

resource "aws_acm_certificate" "alb_cert" {
  domain_name       = var.alb_domain_name
  validation_method = "DNS"

  tags = {
    "Name"        = "${var.app_name}-${var.env_name}-alb-acm"
    "Environment" = "${var.app_name}-${var.env_name}"
    "Application" = var.app_name
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_route53_record" "alb_domain_record" {
  # provider = aws.nonprod
  for_each = {
    for dvo in aws_acm_certificate.alb_cert.domain_validation_options : dvo.domain_name => {
      name   = dvo.resource_record_name
      record = dvo.resource_record_value
      type   = dvo.resource_record_type
    }
  }

  allow_overwrite = true
  name            = each.value.name
  records         = [each.value.record]
  ttl             = 60
  type            = each.value.type
  zone_id         = data.aws_route53_zone.domain.zone_id
}

resource "aws_acm_certificate_validation" "alb_certificate_validation" {
  certificate_arn         = aws_acm_certificate.alb_cert.arn
  validation_record_fqdns = [for record in aws_route53_record.alb_domain_record : record.fqdn]
}