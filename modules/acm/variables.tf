variable "env_name" {
  description = "Name of the application's environment"
}
variable "app_name" {
  description = "Name of the project"
  default     = "cd-template"
}
variable "alb_domain_name" {
  description = "domain name for the ALB's host header"
}
variable "main_domain_name" {
  description = "Main donain name from the Route 53"
}
variable "wildcard_domain_name" {
  description = "Cloudfront domain name"
}