resource "aws_cloudwatch_log_group" "backend_logs" {
  name = "${var.app_name}-${var.env_name}-backend-logs"

  tags = {
    "Name"        = "${var.app_name}-${var.env_name}-backend-logs"
    "Environment" = "${var.app_name}-${var.env_name}"
    "Application" = var.app_name
  }
}

resource "aws_cloudwatch_log_metric_filter" "backend_filter" {
  name           = "MyAppAccessCount"
  pattern        = "error"
  log_group_name = aws_cloudwatch_log_group.backend_logs.name

  metric_transformation {
    name      = "${var.app_name}-${var.env_name}-backend-error"
    namespace = "${var.app_name}-${var.env_name}-backend-error"
    value     = "1"
  }
}