variable "env_name" {
  description = "Name of the application's environment"
}
variable "app_name" {
  description = "Name of the project"
}
variable "service_port" {
  description = "Port which the application is exposed"
  default     = 9000
}
variable "db_port" {
  description = "Port which the application is exposed"
  default     = 3306
}
variable "vpc_id" {
  description = "VPC id which the security groups are going to launch"
}