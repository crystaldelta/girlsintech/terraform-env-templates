resource "aws_security_group" "alb_sg" {
  name        = "${var.app_name}-${var.env_name}-alb-sg"
  description = "Allow service inbound traffic"
  vpc_id      = var.vpc_id

  ingress {
    description = "Allow HTTPS"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "Allow HTTP"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    "Name"        = "${var.app_name}-${var.env_name}-alb-sg"
    "Environment" = "${var.app_name}-${var.env_name}"
    "Application" = var.app_name
  }
}

resource "aws_security_group" "app_sg" {
  name        = "${var.app_name}-${var.env_name}-app-sg"
  description = "Security group for service"
  vpc_id      = var.vpc_id

  ingress {
    description     = "Allow ALB to access service"
    from_port       = 9000
    to_port         = 9000
    protocol        = "tcp"
    security_groups = [aws_security_group.alb_sg.id]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    "Name"        = "${var.app_name}-${var.env_name}-app-sg"
    "Environment" = "${var.app_name}-${var.env_name}"
    "Application" = var.app_name
  }
}

resource "aws_security_group" "rds_sg" {
  name        = "${var.app_name}-${var.env_name}-rds-sg"
  description = "Security group for Database"
  vpc_id      = var.vpc_id

  ingress {
    description     = "Allow Bastion access"
    from_port       = 3306
    to_port         = 3306
    protocol        = "tcp"
    security_groups = [aws_security_group.app_sg.id]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    "Name"        = "${var.app_name}-${var.env_name}-rds-sg"
    "Environment" = "${var.app_name}-${var.env_name}"
    "Application" = var.app_name
  }
}
