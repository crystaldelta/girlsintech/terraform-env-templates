resource "aws_ecs_cluster" "app_cluster" {
  name = "${var.app_name}-${var.env_name}-cluster"

  tags = {
    "Name"        = "${var.app_name}-${var.env_name}-cluster"
    "Environment" = "${var.app_name}-${var.env_name}"
    "Application" = var.app_name
  }
}

resource "aws_ecs_task_definition" "backend" {
  family                   = "${var.app_name}-${var.env_name}-backend"
  network_mode             = "awsvpc"
  requires_compatibilities = ["FARGATE"]
  cpu                      = var.fargate_cpu
  memory                   = var.fargate_memory
  execution_role_arn       = var.task_execution_role_arn
  container_definitions    = <<DEFINITION
    [
      {
        "name":"${var.app_name}-${var.env_name}-backend",
        "memory_reservation": "${var.soft_memory_reservation}",
        "image":"${var.backend_ecr}:${var.tag}",
        "logConfiguration":{
            "logDriver":"awslogs",
            "options":{
              "awslogs-group":"${var.backend_logs}",
              "awslogs-region":"${var.aws_region}",
              "awslogs-stream-prefix":"ecs"
            }
        },
        "environment": [{
          "name": "NODE_ENV",
          "value": "production"
        }],
        "portMappings":[
            {
              "containerPort":9000,
              "hostPort":9000,
              "protocol":"tcp"
            }
        ],
        "essential":true,
        "secrets": [
          {
            "name": "DB_USER",
            "valueFrom": "${var.secret_manager_arn}:DB_USER::"
          },
          {
            "name": "DB_PASS",
            "valueFrom": "${var.secret_manager_arn}:DB_PASS::"
          },
          {
            "name": "DB_NAME",
            "valueFrom": "${var.secret_manager_arn}:DB_NAME::"
          },
          {
            "name": "DB_HOST",
            "valueFrom": "${var.secret_manager_arn}:DB_HOST::"
          },
          {
            "name": "DB_DIALECT",
            "valueFrom": "${var.secret_manager_arn}:DB_DIALECT::"
          },
          {
            "name": "DB_PORT",
            "valueFrom": "${var.secret_manager_arn}:DB_PORT::"
          },
          {
            "name": "APP_HOST",
            "valueFrom": "${var.secret_manager_arn}:APP_HOST::"
          },
          {
            "name": "APP_PORT",
            "valueFrom": "${var.secret_manager_arn}:APP_PORT::"
          }
        ]
      }
  ]
  DEFINITION
}

resource "aws_ecs_service" "backend" {
  name            = "${var.app_name}-${var.env_name}-backend-service"
  depends_on      = [var.ecs_policy]
  cluster         = aws_ecs_cluster.app_cluster.id
  launch_type     = "FARGATE"
  desired_count   = var.desired_count
  task_definition = aws_ecs_task_definition.backend.arn

  load_balancer {
    target_group_arn = var.backend_tg
    container_name   = "${var.app_name}-${var.env_name}-backend"
    container_port   = 9000
  }

  network_configuration {
    security_groups = [var.app_sg]
    subnets         = [var.private_subnet_1, var.private_subnet_2]
  }
}