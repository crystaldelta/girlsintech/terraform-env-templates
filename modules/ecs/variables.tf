variable "env_name" {
  description = "Name of the application's environment"
}
variable "app_name" {
  description = "Name of the project."
}
variable "fargate_cpu" {
  description = "The number of cpu units used by the task."
}
variable "fargate_memory" {
  description = "The amount (in MiB) of memory used by the task."
}
variable "task_execution_role_arn" {
  description = "IAM task execution role"
}
variable "soft_memory_reservation" {
  description = "The soft limit (in MiB) of memory to reserve for the container."
}
variable "backend_ecr" {
  description = "ECR image arn for backend task definition."
}
variable "tag" {
  description = "ECR image tag."
  default     = "latest"
}
variable "backend_logs" {
  description = "Backend cloudwatch logs."
}
variable "aws_region" {
  description = "Cloudwatch log region."
  default     = "ap-southeast-2"
}
variable "ecs_policy" {
  description = "ECS IAM policy."
}
variable "desired_count" {
  description = "Number of instances of the task definition to place and keep running."
}
variable "backend_tg" {
  description = "Backend target group ID."
}
variable "app_sg" {
  description = "Application security group."
}
variable "private_subnet_1" {
  description = "Private subnet ID for the ECS service"
}
variable "private_subnet_2" {
  description = "Private subnet ID for the ECS service"
}
variable "secret_manager_arn" {
  description = "Secret Manager ARN"
}