output "ecs_cluster" {
  value = aws_ecs_cluster.app_cluster.arn
}
output "backend_task_definition" {
  value = aws_ecs_task_definition.backend
}
output "backend_service" {
  value = aws_ecs_service.backend
}