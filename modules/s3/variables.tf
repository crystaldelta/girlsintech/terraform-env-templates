variable "env_name" {
  description = "Name of the application's environment"
}
variable "app_name" {
  description = "Name of the project"
}

variable "cdn_identity_access" {
  description = "Cloudfront Identiy Access arn"
}