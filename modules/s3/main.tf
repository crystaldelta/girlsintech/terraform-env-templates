resource "aws_s3_bucket" "cf_logs" {
  bucket = "${var.app_name}-${var.env_name}-cf-logs"
  acl    = "private"

  tags = {
    "Name"        = "${var.app_name}-${var.env_name}-cf-logs"
    "Environment" = "${var.app_name}-${var.env_name}"
    "Application" = var.app_name
  }
}

resource "aws_s3_bucket" "s3_frontend" {
  bucket = "${var.app_name}-${var.env_name}-static-contents"
  acl    = "private"

  versioning {
    enabled = true
  }

  tags = {
    "Name"        = "${var.app_name}-${var.env_name}-static-contents"
    "Environment" = "${var.app_name}-${var.env_name}"
    "Application" = var.app_name
  }

#   policy = <<EOF
# {
#   "Version": "2008-10-17",
#   "Statement": [
#     {
#       "Sid": "PublicReadForGetBucketObjects",
#       "Effect": "Allow",
#       "Principal": {
#         "AWS": "*"
#       },
#       "Action": "s3:GetObject",
#       "Resource": "arn:aws:s3:::${var.app_name}-${var.env_name}-static-contents/*"
#     }
#   ]
# }
# EOF

  website {
    index_document = "index.html"
    error_document = "index.html"
  }
}

data "aws_iam_policy_document" "s3_policy" {
  statement {
    actions   = ["s3:GetObject"]
    resources = ["${aws_s3_bucket.s3_frontend.arn}/*"]

    principals {
      type        = "AWS"
      identifiers = [var.cdn_identity_access]
    }
  }
}

resource "aws_s3_bucket_policy" "cdn_access_identity_policy" {
  bucket = aws_s3_bucket.s3_frontend.id
  policy = data.aws_iam_policy_document.s3_policy.json
}
