variable "env_name" {
  description = "Name of the application's environment"
}
variable "app_name" {
  description = "Name of the project"
}
variable "s3_static" {
  description = "URL of the S3 bucket"
}
variable "frontend_domain_name" {
  description = "Frontend domain name"
}
variable "api_domain_name" {
  description = "Cloudfront domain name"
}
variable "alb_domain_name" {
  description = "domain name for the ALB's host header"
}
variable "nv_cf_certificate" {
  description = "N.V ACM certificate."
}
variable "cf_logs_s3_bucket" {
  description = "Cloudfront log bucket."
}

variable "main_domain_name" {
  description = "Main Host name"
}
