variable "env_name" {
  description = "Name of the application's environment"
}
variable "app_name" {
  description = "Name of the project"
}
variable "vpc_cidr" {
  description = "VPC CIDR value"
}
variable "az_count" {
  description = "Number of availablity zones"
  default     = 2
}