# Girls In Tech DevOps WorkShop

## 49 Release / 0 Incidents

1. Good Requirements
1. Unit Testing
1. Good planning - realistic timelines and expectations
1. Functional Testing
1. System Testing
1. Automated UI Testing
1. Monitoring and Alerting
1. Good Centralised Logging
1. Team Accountability
1. Continuous Delivery Small changes promoted to production regularly
1. DevOps Processes including Infrastructure As Code


## Why Infra As Code

Because as developers, we are lazy!

ClickOps leads to problems, late night calls, doing the same menial tasks over and over again...yawn....

DevOps leads to a happy life. Everything automated. You can run your scripts and go for coffee, chat on slack to your friends, and get more work done with less effort. Your boss will think you are actually working, but its the power of the CPU that is actually doing all the work!

## Infra As Code Tech Choices

There are many:

1. AWS CloudFormation
1. AWS CDK
1. Azure ARM
1. Terraform
1. Pulumi
1. Ansible
1. Chef
1. Puppet
1. etc...

## Terraform Primer

https://learn.hashicorp.com/collections/terraform/aws-get-started

 
## Commands

** One-time setup MaxOS HomeBrew**

1. Install Terraform

https://learn.hashicorp.com/collections/terraform/aws-get-started

**Windows / Linux**
https://www.terraform.io/downloads.html

**Mac**
```
brew install terraform
```


2. Verify Terraform
```
terraform -v
```

3. Install AWS CLI
```
$ curl "https://awscli.amazonaws.com/AWSCLIV2.pkg" -o "AWSCLIV2.pkg"
$ sudo installer -pkg AWSCLIV2.pkg -target /
```
4. Verify AWS Install
```
$ which aws
aws --version
```
5. clone me (or maybe you already have...) https://gitlab.com/crystaldelta/girlsintech/terraform-env-templates

```sh
git clone https://gitlab.com/crystaldelta/girlsintech/terraform-env-templates.git
```

6. Create AWS CLI Profile

```sh
$ aws configure --profile girlsintech-workshop
AWS Access Key ID [None]: AKIA2LWN35YQTGNJIRMK
AWS Secret Access Key [None]: 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
Default region name [None]: ap-southeast-2
Default output format [None]: json

$ export AWS_PROFILE=girlsintech-workshop
```

7. Login to AWS Console:

https://girlsintech-workshop.signin.aws.amazon.com/console


Choose: IAM User

account alias: `girlsintech-workshop`

username: `dmack`

password: aN\9F`cQ^;e)4!&,Qbq]yfR8


8. Update the following variables in

`terraform-env-templates/env/staging/main.tf`

```sh
backend "s3" {
    bucket         = "cd-girls-in-tech-iac-tfstate"
    key            = "cd-terraform/env/<REPLACE_ME|dmack-git-demo>/terraform.tfstate"
    region         = "ap-southeast-2"
    dynamodb_table = "girls-in-tech-tfstate-locks"
    encrypt        = false
  }
```

`terraform-env-templates/env/staging/variables.tf`

Replace all variables with the `<REPLACE_ME|>` token

1. app_name - e.g. `<your_short_name>-app`
1. env_name - e.g. `dev`
1. vpc_cidr - e.g. `102`
1. alb_domain_name - e.g. `${app_name}-${env_name}`
1. api_domain_name - `${app_name}-${env_name}`


9. You're ready to rock and roll...Run it!!

```sh
$ cd ./env/staging

$ terraform init

$ terraform plan

$ terraform apply

set mysql db password: eg: 9h2.T=h9~5,$u=^P*V
set mysql user: eg: dmack_git_user
```

10. Copy the Front End Add to s3 (to be served via CloudFront)

```sh
aws s3 cp ../../docs/index.html s3://dmack-git-dev-static-contents/
```

11. You now need to build your express app and push to ECR

You'll need to install docker:

https://docs.docker.com/docker-for-mac/install/
https://docs.docker.com/docker-for-windows/install/
https://docs.docker.com/engine/install/ubuntu/


```sh
git clone https://gitlab.com/crystaldelta/girlsintech/girlsintech-awesome-app.git

cd girlsintech-awesome-app

aws ecr get-login-password --region ap-southeast-2 | docker login --username AWS --password-stdin 712322575905.dkr.ecr.ap-southeast-2.amazonaws.com

docker build -t girlsintech-awesome-app .
docker tag girlsintech-awesome-app:latest 712322575905.dkr.ecr.ap-southeast-2.amazonaws.com/<app_name>-<env_name>-backend:latest
docker push 712322575905.dkr.ecr.ap-southeast-2.amazonaws.com/<app_name>-<env_name>-backend:latest
```

11. Add secrets into secrets manager:

```
DB_USER=dmack_git_user
DB_PASS=x.g.9h2.T=h9~5,$u=^P*V
DB_NAME=anyvalue
DB_HOST=anyvalue
DB_DIALECT=mysql
DB_PORT=3306
APP_HOST=https://<app_name>-<env_name>.girlsintech.crystaldelta.net
APP_PORT=9000
```

11. On next ECS container restart Now you should be live!!! Check out your handy work!!!

https://<app_name>-<env_name>.girlsintech.crystaldelta.net/

12. Finished....time to destory :-(

```sh
$ terraform destroy
```


### Directories

| Directory Name    | Description                                                                                           |
|-------------------|-------------------------------------------------------------------------------------------------------|
| `env`             | Code to build the Infrastructure for *Staging*, *Production*, and Admin services environments.        |
| `modules`         | Modules for building DNS records.                                                                     |


## Prepare config & Create AWS Platform

Setup the following for each env build

1. Get the AWS Access Key and Secret Access token of your account and configure it in aws.tf which will acquire the AWS access with Terraform to deploy the resources.
2. Create a .gitignore file & ignore the below mentioned terraform runtime generated folders and files.

```
.terraform
*.tfstate
*.tfstate.*
*.tfvars
```

### Steps to create a new environment using Terraform modules 

1. Navigate to the `env` repository and into the environment-specific repository (Eg: production)

2. Make sure you have the AWS account access_keys and Secret access tokens with enough permissions to create the AWS services.

3. Configure the AWS credentials in your local environment.

```
aws configure --profile #profile_name (I'm naming it as dmack_stg)
AWS Access Key ID [None]: #paste your AWS access key
AWS Secret Access Key [None]: #paste your secret access key
Default region name [None]: #Enter the region name that you want to create the environment. (eg: ap-southeast-2)
Default output format [None]: #ENter the output format that you want to return. (eg: json)
```

```sh
$ aws configure --profile girlsintech-workshop
AWS Access Key ID [None]: AKIA2LWN35YQTGNJIRMK
AWS Secret Access Key [None]: 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
Default region name [None]: ap-southeast-2
Default output format [None]: json

$ export AWS_PROFILE=girlsintech-workshop
```

4. Finally export the AWS Profile that you created now.

```
export AWS_PROFILE=#profile_name
```

5. Once you configure the AWS profile the configured credentials will be stored in the `~/.aws/credentials` file in the name of your profile and exported `AWS_PROFILE` environment variable.

6. Go to `.env/staging` repo and change the `profile` with your profile name that you gave during the `aws configure` (eg: profile = "dmack-terrific-env")

7. Configure Terraform Backend to safely store the `.tfstate` in the S3 bucket. So your team members can work on the scripts without losing the latest service builds. For more details follow this link: https://www.terraform.io/docs/language/settings/backends/index.html. `Note: If you are still learning how to use Terraform, we recommend using the default local backend, which requires no configuration.` In that case, ignore the `terraform {...}` configuration in your main.tf file inside your environment repo.

8. Call the terraform modules in the `source`. If you want to create VPC you can call the VPC module by giving the repo path `../../modules/vpc` likewise you can call the multiple modules from multiple sources. (Eg:`../../modules/module_name`) For more details follow this link: https://www.terraform.io/docs/language/modules/index.html.

9. Once you called the modules it's time to create the environment. 
`Note: Before creating the stacks we need to make sure that we are creating the stacks in the correct order because the stacks are dependent on each other. (Eg: RDS service is dependent on the VPC and Security stacks so before creating the RDS we need to make sure we have created the required stacks on which the new services are dependent on it)

### Lets deploy the Infrastructure
1. Initialise the terraform in your environment [./env/production]

`$ terraform init`

2. You can list the terraform state to see what are all the service stacks and sub-stacks that you are going to create.

`$ terraform state list`

3. Now, let us create a plan for the VPC (Virtual Private Cloud) stack, as I said about we need to create the stacks to avoid dependency issues.

`$ terraform plan -target=module.vpc  //Or if you want to create a specific service inside the module run $ terraform plan -target=module.aws_vpc.main (point the resource name in the module)`

4. Once you are satisfied with your service, deploy the scripts by running the apply command.

`$ terraform apply -target=module.vpc OR $ terraform apply -target=module.aws_vpc.main`

5. It will again list the services that you are going to create and it will ask for user confirmation. you need to enter [Yes] to deploy the terraform scripts. 

6. Now, let's create a plan for the Security stack.

`$ terraform plan -target=module.security OR terraform plan -target=module.security.alb_sg //For a specific service.`

7. Once you are satisfied with your service deploy the scripts by running the apply command.

`$ terraform apply -target=module.security OR terraform apply -target=module.security.alb_sg`

8. Now, let's create a plan for the IAM (Identity Access Management) stack and deploy it after reviewing the plan. 

`$ terraform plan -target=module.iam`

* Once you reviewed the plan let's deploy,

`$ terraform apply -target=module.iam`

9. Now, let's create a plan for the ACM (AWS Certificate Manager) stack and deploy it after reviewing the plan. Remember you can plan/deploy specific service by following the example given in point No.3.

`$ terraform plan -target=module.acm`

* Once you reviewed the plan let's deploy,

`$ terraform apply -target=module.acm`

10. Now, let's create a plan for the ALB (Application Load Balancer) stack and deploy it after reviewing the plan. 

`$ terraform plan -target=module.alb`

* Once you reviewed the plan let's deploy,

`$ terraform apply -target=module.alb`

11. Now, let's create a plan for the S3 (Simple Storage Service) stack and deploy it after reviewing the plan. 

`$ terraform plan -target=module.s3`

* Once you reviewed the plan let's deploy,

`$ terraform apply -target=module.s3`

12. Now, let's create a plan for the Secret Manager stack and deploy it after reviewing the plan. 

`$ terraform plan -target=module.secret_manager`

* Once you reviewed the plan let's deploy,

`$ terraform apply -target=module.secret_manager`

13. Now, let's create a plan for the CloudWatch stack and deploy it after reviewing the plan. 

`$ terraform plan -target=module.cloudwatch`

* Once you reviewed the plan let's deploy

`$ terraform apply -target=module.cloudwatch`

14. Now, let's create a plan for the RDS (Relational Database Service) stack and deploy it after reviewing the plan. 

`$ terraform plan -target=module.rds`

```Now, it will ask for the RDS username and password 
Enter the user name and password for the DB and store the credentials in a secure place, If you lose the credentials we cannot able to retrieve them back.
```
* Once you reviewed the plan let's deploy

`$ terraform apply -target=module.rds`

15. Now, let's create a plan for the ECR (Elastic Container Registry) stack and deploy it after reviewing the plan. 

`$ terraform plan -target=module.ecr`

* Once you reviewed the plan let's deploy

`$ terraform apply -target=module.ecr`


16. Now, let's create a plan for the ECS (Elastic Container Service) stack and deploy it after reviewing the plan. 

`$ terraform plan -target=module.ecs`

* Once you reviewed the plan let's deploy

`$ terraform apply -target=module.ecs`
```