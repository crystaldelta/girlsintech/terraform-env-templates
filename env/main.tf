provider "aws" {
  region                  = "ap-southeast-2"
  shared_credentials_file = "/Users/$USER/.aws/credentials"
  profile                 = "cd-sandbox"
}
#S3 bUCKET FOR STORING THE TERRAFORM STATE
resource "aws_s3_bucket" "terraform_state" {
  bucket = "cd-terraform-tfstate"
  versioning {
    enabled = true
  }
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }
}

#DINAMODB TABLE FOR LOCKING THR TERRAFORM
resource "aws_dynamodb_table" "terraform_locks" {
  name         = "cd-terraform-tfstate-locks"
  billing_mode = "PAY_PER_REQUEST"
  hash_key     = "LockID"  
  
attribute {
    name = "LockID"
    type = "S"
  }
}