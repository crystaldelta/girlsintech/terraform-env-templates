variable "private_subnet" {
  default = "subnet-035f6b939ac1710fe"
}
variable "vpc_id" {
  default = "vpc-0837804b4eaac8a34"
}
variable "public_subnet_1" {
  default = "subnet-0d159164421f7727f"
}
variable "public_subnet_2" {
  default = "subnet-0468ffb39ba02535d"
}
variable "main_domain_name" {
  default = "girlsintech.crystaldelta.net"
}
variable "jenkins_domain_name" {
  default = "jenkins-pms.girlsintech.crystaldelta.net"
}
variable "jenkins_ec2_keypair" {
  default = "girlsintech-jenkins"
}
variable "bastion_ec2_keypair" {
  default = "girlsintech-bastion"
}

variable "app_name" {
  default = "dmack-git"
}
