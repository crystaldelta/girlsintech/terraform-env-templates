provider "aws" {
  region                  = "ap-southeast-2"
  shared_credentials_file = "/Users/$USER/.aws/credentials"
  profile                 = "cd-sandbox"
}

terraform {
  required_version = ">= 0.14"

  backend "s3" {
    bucket         = "cd-girls-in-tech-tfstate"
    key            = "cd-terraform/env/dmack-admin/terraform.tfstate"
    region         = "ap-southeast-2"
    dynamodb_table = "girls-in-tech-tfstate-locks"
    encrypt        = false
  }
}
########################Admin Modules########################
module "jenkins_server" {
  source              = "../../modules/admin/jenkins"
  vpc_id              = var.vpc_id
  private_subnet      = var.private_subnet
  public_subnet_1     = var.public_subnet_1
  public_subnet_2     = var.public_subnet_2
  main_domain_name    = var.main_domain_name
  bastion_ec2_sg      = module.bastion_server.bastion_ec2_sg
  jenkins_ec2_keypair = var.jenkins_ec2_keypair
  jenkins_domain_name = var.jenkins_domain_name
  app_name            = var.app_name
}

module "bastion_server" {
  source              = "../../modules/admin/bastion_server"
  vpc_id              = var.vpc_id
  public_subnet_1     = var.public_subnet_1
  public_subnet_2     = var.public_subnet_2
  bastion_ec2_keypair = var.bastion_ec2_keypair
  app_name            = var.app_name
}