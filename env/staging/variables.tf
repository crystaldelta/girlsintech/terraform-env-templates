variable "app_name" {
  description = "Name of the Application"
  default     = "<REPLACE_ME|dmack-git>"
}
variable "env_name" {
  description = "Name of the application's environment"
  default     = "<REPLACE_ME|demo>"
}
variable "default_region" {
  description = "Application's default region"
  default     = "ap-southest-2"
}
variable "vpc_cidr" {
  description = "VPC CIDR"
  default     = "10.<REPLACE_ME|0-255>.0.0/16"
}
variable "alb_domain_name" {
  description = "ALB DNS name"
  default     = "alb-<REPLACE_ME|dmack-git-demo>.girlsintech.crystaldelta.net"
}
variable "main_domain_name" {
  description = "Main Host name"
  default     = "girlsintech.crystaldelta.net"
}
variable "wildcard_domain_name" {
  description = "Cloudfront domain name"
  default     = "*.girlsintech.crystaldelta.net"
}
variable "api_domain_name" {
  description = "Backend API domain name"
  default     = "api-<REPLACE_ME|dmack-git-demo>.girlsintech.crystaldelta.net"
}
variable "mysql_master_user" {
  description = "Mysql DB username"
}
variable "mysql_master_password" {
  description = "Mysql DB password"
}

variable "allocated_storage" {
  description = "RDS Allocated Storage"
  default     = 20
}