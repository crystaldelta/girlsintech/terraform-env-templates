provider "aws" {
  region = "ap-southeast-2"
}

provider "aws" {
  region = "us-east-1"
  alias  = "east"
}

terraform {
  required_version = ">= 0.13"

  backend "s3" {
    bucket         = "cd-girls-in-tech-iac-tfstate"
    key            = "cd-terraform/env/dmack-dev/terraform.tfstate"
    region         = "ap-southeast-2"
    dynamodb_table = "girls-in-tech-tfstate-locks"
    encrypt        = false
  }
}
########################Modules########################
module "vpc" {
  source   = "../../modules/vpc"
  env_name = var.env_name
  app_name = var.app_name
  vpc_cidr = var.vpc_cidr
}

module "security" {
  source   = "../../modules/security"
  env_name = var.env_name
  app_name = var.app_name
  vpc_id   = module.vpc.vpc_id
}

module "iam" {
  source             = "../../modules/iam"
  env_name           = var.env_name
  app_name           = var.app_name
  secret_manager_arn = module.secret_manager.secret_manager_arn.arn
}

module "acm" {
  source               = "../../modules/acm"
  env_name             = var.env_name
  app_name             = var.app_name
  alb_domain_name      = var.alb_domain_name
  main_domain_name     = var.main_domain_name
  wildcard_domain_name = var.wildcard_domain_name
}

module "alb" {
  source              = "../../modules/alb"
  env_name            = var.env_name
  app_name            = var.app_name
  vpc_id              = module.vpc.vpc_id
  public_subnet_1     = module.vpc.public_subnet_1
  public_subnet_2     = module.vpc.public_subnet_2
  alb_sg              = module.security.alb_sg
  alb_acm_certificate = module.acm.alb_acm_certificate
  alb_domain_name     = var.alb_domain_name
}

module "s3" {
  source   = "../../modules/s3"
  env_name = var.env_name
  app_name = var.app_name
  cdn_identity_access = module.cloudfront.cdn_identity_access
}

module "secret_manager" {
  source   = "../../modules/secret_manager"
  env_name = var.env_name
  app_name = var.app_name
}

module "cloudwatch" {
  source   = "../../modules/cloudwatch"
  env_name = var.env_name
  app_name = var.app_name
}

module "rds" {
  source                = "../../modules/rds"
  env_name              = var.env_name
  app_name              = var.app_name
  db_app_name           = var.app_name
  db_subnet_1           = module.vpc.db_subnet_1
  db_subnet_2           = module.vpc.db_subnet_2
  db_sg                 = module.security.rds_sg
  mysql_master_user     = var.mysql_master_user
  mysql_master_password = var.mysql_master_password
  allocated_storage     = var.allocated_storage
}

module "ecr" {
  source   = "../../modules/ecr"
  env_name = var.env_name
  app_name = var.app_name
}

module "ecs" {
  source                  = "../../modules/ecs"
  env_name                = var.env_name
  app_name                = var.app_name
  fargate_cpu             = 256
  fargate_memory          = 512
  desired_count           = 1
  task_execution_role_arn = module.iam.task_execution_role.arn
  soft_memory_reservation = 256
  backend_ecr             = module.ecr.backend_ecr.repository_url
  backend_logs            = module.cloudwatch.backend_logs
  ecs_policy              = module.iam.ecs_policy
  backend_tg              = module.alb.backend_tg_id.arn
  app_sg                  = module.security.app_sg
  private_subnet_1        = module.vpc.private_subnet_1
  private_subnet_2        = module.vpc.private_subnet_2
  secret_manager_arn      = module.secret_manager.secret_manager_arn.arn
}

module "cloudfront" {
  source               = "../../modules/cloudfront"
  env_name             = var.env_name
  app_name             = var.app_name
  s3_static            = module.s3.s3_frontend
  main_domain_name     = var.main_domain_name
  frontend_domain_name = "${var.app_name}-${var.env_name}.${var.main_domain_name}"
  api_domain_name      = var.api_domain_name
  alb_domain_name      = var.alb_domain_name
  nv_cf_certificate    = module.acm.wildcard_acm_certificate
  cf_logs_s3_bucket    = module.s3.cf_logs_s3_bucket.id
}