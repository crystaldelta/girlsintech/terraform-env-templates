### Overview

This repository holds different AWS environment-specific repository. 

### Directories

| Directory Name    | Description                                                                                           |
|-------------------|-------------------------------------------------------------------------------------------------------|
| `admin`           | Holds the Admin/Global components like Jenkins server, Bastion server.                                |
| `staging`         | Holds the Staging environment launch functions.                                                       |
| `production`      | Holds the Production environment launch functions.                                                    |
|---------------------------------------------------------------------------------------------------------------------------|
