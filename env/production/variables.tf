variable "app_name" {
  description = "Name of the Application"
  default     = "dmack-git"
}
variable "env_name" {
  description = "Name of the application's environment"
  default     = "prod"
}
variable "default_region" {
  description = "Application's default region"
  default     = "ap-southeast-2"
}
variable "vpc_cidr" {
  description = "VPC CIDR"
  default     = "10.10.0.0/16"
}
variable "alb_domain_name" {
  description = "ALB DNS name"
  default     = "alb-girlsintech-prod.girlsintech.crystaldelta.net"
}
variable "main_domain_name" {
  description = "Main Host name"
  default     = "girlsintech.crystaldelta.net"
}
variable "wildcard_domain_name" {
  description = "Cloudfront domain name"
  default     = "*.girlsintech.crystaldelta.net"
}
variable "mysql_master_user" {
  description = "Mysql DB username"
}
variable "mysql_master_password" {
  description = "Mysql DB password"
}
variable "allocated_storage" {
  description = "Size of the RDS DB"
  default     = 30
}