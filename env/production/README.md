### Overview

This repository will create a template AWS environment.

### Directories

| Filename          | Description                                                                                           |
|-------------------|-------------------------------------------------------------------------------------------------------|
| `main.tf`         | Terraform script for creating PMS Production environment.                                             |
| `variables.tf`    | Holds the variables which will be passed to main.tf.                                                  |
|---------------------------------------------------------------------------------------------------------------------------|
